---
title: Petit glossaire de termes informatiques
author: Commission Inter-IREM Informatique
date: version du \today{}
lang: fr-FR
---

Ce glossaire de termes informatiques à l'intention des enseignants du
secondaire est un travail en cours publié en l'état.

Une version en ligne ainsi que les fichiers source sont accessibles
depuis [`framagit.org/c3i/glossaire/wikis`](https://framagit.org/c3i/glossaire/wikis). 

