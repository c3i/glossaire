# Petit glossaire de termes informatiques 


Ce glossaire de termes informatiques à l'intention des enseignants du
secondaire est un travail en cours de la c3i - Commission Inter-IREM
Informatique -,  publié en l'état.

Le glossaire proprement dit est accessible depuis le wiki attaché à ce
projet GitLab \
→ [framagit.org/c3i/glossaire/wikis](https://framagit.org/c3i/glossaire/wikis/home). 

Une version PDF est fournie \
→ [framagit.org/c3i/glossaire/.../glossaire.pdf](https://framagit.org/c3i/glossaire/-/blob/master/build/glossaire.pdf)

## Organisation du projet

Le wiki contient le glossaire lui-même, à raison d'une page Markdown par
terme.

Une version PDF est produite à partir de ces mêmes fichiers Markdown.

On trouve ici 

* `Readme.md` - vous y êtes...
* `TODO.md` - liste de termes en travaux, à inclure 
* `Makefile` - principalement pour contruire la version PDF via pandoc
* `glossaire.md` - entête de la version PDF 

Le wiki contient aussi un fichier

* `Home.md` - page d'accueil du wiki, donc de la version en ligne du
  glossaire. 

On trouve par aileurs

* `oldies/` - des vieillerie, pour mémoire. État du projet avant de
  passer à sa _wikifisation_. 


## À l'intention des rédacteurs

Au menu :

* organisation du glossaire 
* syntaxe
* quelques notes et remarques 

## → organisation du glossaire

Un terme du glossaire correspond à une page du wiki.  
Cette page porte le nom de l'entrée, sans majuscule initiale.
Le nom de la page ne comporte pas de lettre accentuée (remplacée par la lettre sans accent — les principales motivations sont le respect de l'ordre alphabétique des entrées dans le wiki, et le codage des caractères des noms de fichiers, même si UTF-8...).
Le nom de la page comporte éventuellement des caractères espaces (et des parenthèses).
Les éventuelles lettres apostrophes sont remplacées par des espaces.

L'ensemble des entrées est listé dans la colone de droite du GitLab.  
(Une table des entrées pourait être générée.)

Une référence à une entrée est possible depuis n'importe quelle page sous la forme `[texte libre](nom de l entree)`. 


## → syntaxe — sur la forme

La page d'un terme débute par un titre de niveau 2 avec le nom du terme, par exemple

    ## Allocation mémoire

------------------------------

Les **références vers une entrée du glossaire** s'écrivent de la manière suivante :

    Le pluriel d'[adresse](adresse) s'écrit [adresses](adresse). 

rendu

Le pluriel d'[adresse](adresse) s'écrit [adresses](adresse). 

------------------------------

Les **notes**, remarques, todo **à destination des rédacteurs** sont saisis comme suit :

	> **Note** Une note

ou 

	> **TODO** Un truc à faire...

rendus

> **Note** Une note

et

> **TODO** Un truc à faire...

------------------------------

Les **exemples** sont saisis comme suit :

	**Exemple.** Un exemple... 

rendu

**Exemple.** Un exemple...

------------------------------

## → quelques notes et remarques 

> ajouter (systématiquement) des exemples ?

> ultimement, ajouter des définitions pour les élèves

> parler de l'évolution du vocabulaire pour mentionner des mots dont l'usage a pu changer (exemple pour fonction: routine, sous-routine, sous-programme, voire macro, etc.)

> Passer à une syntaxe type [_fenced_divs_](https://pandoc.org/MANUAL.html#divs-and-spans) pour les exemples, notes et todo ? 

> **Note** vérifier les notes et exemples... grep is your friend. 

> Contrôler l'ordre des entrés pour mettre [test](../../wikis/test)
> avant [test unitaire](../../wikis/test-unitaire),
> [variable](../../wikis/variable) avant
> [variable globale](../../wikis/variable-globale) et autres...
