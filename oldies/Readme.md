# Vieilleries pour mémoire

* [`glossaire.tex`](glossaire.tex) - version au 13 décembre 2019 du
  fichier partagé via Overleaf suite à adécision d'abandonner ce
  format. Le contenu a été éclaté dans les entrées du projet
  glossaire.wiki.

