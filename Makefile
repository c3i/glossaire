MAIN = glossaire
ENTRIES = adresse affectation algorithme aliasing appel_de_fonction \
	code compilation constante contexte_d_execution \
	declaration definition duree_de_vie \
	effet_de_bord enregistrement environnement_d_execution \
	etat_d_un_objet etat evaluation execution_d_un_programme \
	execution_d_une_instruction  expression \
	fonction \
	implementation_de_fonction implementation interface_de_fonction \
	interface litteral \
	procedure prototype_de_fonction signature_de_fonction \
	signature specification

ENTRY_DIR = ../glossaire.wiki
ENTRY_FILES = $(addsuffix .md, $(addprefix $(ENTRY_DIR)/,$(ENTRIES)))

# config pandoc 
A4PAPER = -V "geometry:a4paper,includeheadfoot,margin=2.34cm"

# main 
build/glossaire.pdf : $(MAIN).md Makefile $(ENTRY_DIR)/[a-z]*.md
	pandoc -s $(A4PAPER) $(MAIN).md $(ENTRY_DIR)/[a-z]*.md -o $@

# misc
.PHONY : clean realclean mrproper
clean :
realclean mrproper : clean
